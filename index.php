<?php include("header.php"); ?>

<body id="bootstrap-overrides">
  <div class="capa">
    <div class="texto-capa">
      <h1>Pilot registration</h1>
    </div>
  </div>
  <main id="content">
    <div class="container" id="line">
      <div class="row">
        <a href="index.html" class="navbar-brand">
          <span class="img-logo_top">FHINCK</span>
        </a>
      </div>
    </div>
    <div class="container">
      <h2>FRONT END CODER WARRIOR.</h2>
      <p class="text-center">Desenvolvimento de soluções elegantes que auxiliam nossos clientes a acompanhar todo o fluxo dos seus processos. Formulários e dashboards
        <br>esponsivos e inteligentes que se comunicam com bases de dados e disparam alertas e mensagens.</p>
      </div>
      <div class="modal fade" tabindex="-1" role="dialog" id="welcomeModal"><!--Modal Welcome-->
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h3 class="modal-title text-center">Welcome Piloto</h3>
            </div>
            <div class="modal-body">
              <p>Bem vindo ao nosso sistema de controle espacial, para podermos continuar com a sua solicitação precisamos
                primeiramente que preencha o campo Indentificação com seu ID que é composto de um dia [0 a 31] + mes [0 a 12] + 5. Exemplo:31125</p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="closeModal" data-dismiss="modal">Ok entendi</button>
              </div>
            </div><!-- /.modal-content -->
          </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
        <div class="container"><!-- Começo Form -->
          <form method="post" action="index.php" id="form-cadastro" enctype="multipart/form-data">
            <div class="form-group"><!--Indentificação-->
              <label for="inputAuth">Digite sua Indentificação</label>
              <input type="text" class="form-control" name="id" id="inputAuth" maxlength="5" placeholder="Digite seu ID" required>
              <div class="alert alert-success" role="alert" id="text-access"><!-- Message Sucess ID -->
                <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
                <span class="sr-only">Sucesso</span>
                Validação completa por favor preencha as informações abaixo.
              </div>

            </div>
            <div class="form-group"><!--E-mail-->
              <label for="inputEmail">Digite o seu e-mail piloto</label>
              <div class="input-group email">
                <span class="input-group-addon">
                  <span class="glyphicon glyphicon-envelope"></span>
                </span>
                <input type="email" class="form-control form-auth" name="email" id="inputEmail" data-erro="É necessário informar um e-mail válido." required placeholder="Digite seu e-mail piloto" disabled>
              </div>
            </div>

            <div class="form-group"><!--Nome-->
              <label for="inputEmail">Digite seu nome piloto</label>
              <div class="input-group name">
                <span class="input-group-addon">
                  <span class="glyphicon glyphicon-user"></span>
                </span>
                <input type="text" class="form-control form-auth" name="nome" id="inputNome" placeholder="Digite seu nome" data-erro="É necessário informar o nome. Lembrando que o nome deve possuir mais de 3 caracteres" required data-toggle="modal" disabled>
              </div>
              <div>
              </div>

              <div class="modal fade" role="dialog" id="racaPiloto"><!-- Modal com info sobre o piloto -->
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header header-nome">
                      <h4 class="modal-title">Raça selecionada foi do Sistema Planetário</h4>
                    </div>
                    <div class="modal-body">
                      <img src="" class="img-responsive">
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                    </div>
                  </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
              </div><!-- /.modal -->
            </div>
            <div class="form-group"><!--Modelo espaçonave-->
              <label for="inputEmail">Informe o modelo de sua nave piloto</label>
              <div class="input-group select">
                <span class="input-group-addon">
                  <span class="glyphicon glyphicon-plane"></span>
                </span>
                <select class="form-control form-auth" name="espaconave" id="nave-opt" data-erro="É necessário selecionar uma espaçonave piloto" disabled required>
                  <option>Selecione um tipo de espaçonave</option>
                  <optgroup label="Tipo Espaçonave">
                    <option name="nave1" value="nave1">B-WING PROTOTYPE (BLADE WING)</option>
                    <option name="nave2" value="nave2">B-WING FIGHTER</option>
                    <option name="nave3" value="nave3">BROKEN HORN</option>
                    <option name="nave4" value="nave4">CLONE Z-95 STARFIGHTER</option>
                    <option name="nave5" value="nave5">CRUCIBLE</option>
                    <option name="nave6" value="nave6">GR-75 MEDIUM TRANSPORT</option>
                    <option name="nave7" value="nave7">GX1 SHORT HAULE</option>
                  </optgroup>
                </select>
                <input type="hidden" id="text-nave" name="select" value="">
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="row">
                  <div class="col-md-12">
                    <div class="img-responsive" id="naveBox">
                      <img src="assets\img\" id="navePhoto">
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="form-group"><!--Motivos atracamento-->
              <label for="checkboxOpt">Escolha uma ou mais motivos do atracamento</label><br>
              <label class="checkbox-inline">
                <input type="checkbox" class="inlineCheckbox" name="motivo" value="fuel_supply" disabled> Abastecimento/Combustível
              </label>
              <label class="checkbox-inline">
                <input type="checkbox" class="inlineCheckbox" name="motivo" value="unload_charge" disabled> Descarregar Carga
              </label>
              <label class="checkbox-inline">
                <input type="checkbox" class="inlineCheckbox" name="motivo" value="fix_eletrica" disabled> Manutenção Elétrica/Eletrônica
              </label>
              <label class="checkbox-inline">
                <input type="checkbox" class="inlineCheckbox" name="motivo" value="fix_propulsores" disabled> Manutenção de Propulsores
              </label>
              <label class="checkbox-inline">
                <input type="checkbox" class="inlineCheckbox" name="motivo" value="verify_system" disabled> Verificação de Sistemas
              </label>
            </div>
            <div class="form-group">
              <label class="checkbox-inline">
                <input type="checkbox" class="inlineCheckbox" name="motivo" value="verify_system" disabled> Verificação de Sistemas
              </label>
              <label class="checkbox-inline">
                <input type="checkbox" class="inlineCheckbox" name="motivo" value="clean_aircraft" disabled> Limpeza da Aeronave
              </label>
            </div>

            <div class="form-inline"><!--Data Aterrisagem-->
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="inputDataD">Data da Aterrisagem</label>
                    <div class="input-group date">
                      <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                      </span>
                      <input type="text" name="dataA" class="form-control form-auth" maxlength="10" data-erro="Por favor piloto digite uma data válida." placeholder="01/01/2017" id="dataA" disabled required>
                    </div>
                  </div><!-- form group -->
                </div><!-- col-md-6 -->
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="inputHoraD">Hora da Aterrisagem</label><!--Hora Decolagem-->
                    <div class="input-group date">
                      <span class="input-group-addon">
                        <span class="glyphicon glyphicon-time"></span>
                      </span>
                      <input type="text" name="horaA" class="form-control form-auth" maxlength="5" data-erro="Por favor piloto digite uma hora válida." placeholder="00:00" id="horaA" disabled required>
                    </div><!-- input-group date -->
                    <div class="alert alert-danger" role="alert" id="text-errohora"><!-- Erro Nome -->
                      <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                      <span class="sr-only">Erro</span>
                      Por favor piloto digite uma hora válida.
                    </div><!-- alert alert-danger -->
                  </div><!-- form group -->
                </div><!-- col-md-6 -->
              </div><!-- row -->

              <br>
              <div class="form-inline"><!--Data Decolagem-->
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="inputDataD">Data da Decolagem&nbsp;&nbsp;</label>
                      <div class="input-group date">
                        <span class="input-group-addon">
                          <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                        <input type="text" name="dataD" class="form-control form-auth" maxlength="10" data-erro="Por favor piloto digite uma data válida." placeholder="01/01/2017" id="dataD" disabled required>
                      </div>
                    </div><!-- form group -->
                  </div><!-- col-md-6 -->
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="inputHoraD">Hora da Decolagem&nbsp;&nbsp;</label><!--Hora Decolagem-->
                      <div class="input-group date">
                        <span class="input-group-addon">
                          <span class="glyphicon glyphicon-time"></span>
                        </span>
                        <input type="text" name="horaD" class="form-control form-auth" maxlength="5" data-erro="Por favor piloto digite uma hora válida." placeholder="00:00" id="horaD" disabled required>
                      </div><!-- input-group date -->
                    </div><!-- form group -->
                  </div><!-- col-md-6 -->
                </div><!-- row -->
                <br>

                <div class="form-group"><!--Selfie Piloto-->
                  <label for="selfiePiloto">Precisamos de uma Selfie piloto para verifição</label>
                  <input type="file" name="selfie" class="selfiePiloto form-auth" data-erro="Piloto precisamos de uma selfie para verificação" id="selfiePiloto" disabled required>
                  <p class="help-block">Max 5mb.</p>
                </div>
                <br>
                <div class="form-group"><!--Botão submit-->
                  <div class="container">
                    <div class="row">
                      <button type="submit" class="btn btn-default" id="sendform" name="sendform" disabled>Solicitar autorização</button>
                    </div>
                  </div>
                </div>
                <div class="modal fade" tabindex="-1" role="dialog" id="modalsend">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Obrigado Piloto por sua mensagem</h4>
                      </div>
                      <div class="modal-body">
                        <p class="text-center">Sua mensagem foi enviada seu número da solicitação é <span id="keyacess"></span></p>
                      </div>
                    </div><!-- /.modal-content -->
                  </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->
              </form>
            </main>
            <?php include("footer.php"); ?>
