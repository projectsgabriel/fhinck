//-----------------------------------------------------------------
// Modal Body
//-----------------------------------------------------------------
$(window).on('load',function(){
  $("#welcomeModal").modal('show');
});
//-----------------------------------------------------------------
// Validador Indentificação
//-----------------------------------------------------------------
$(function() {
  var messageA = $("#text-access");
  var messageB = $("#text-block");
  var inputs = $(".form-auth,.inlineCheckbox,.selfiePiloto,.btn");
  $('#inputAuth').change(function () {
    var idInput = $(this).val();
    var id = /^(0[1-9]|[12][0-9]|3[01])(0[1-9]|1[0-2])5/;
    if (id.test(idInput)) {
      $(this).css('border', '1px solid green');
      messageA.show();
      messageB.hide();
      inputs.prop("disabled", false);
    }else if($(this).is(':empty')|| $(this).val().length < 5 ) {
      $(this).css('border', '1px solid red');
      messageB.show();
      messageA.hide();
      inputs.prop("disabled", true);
    }
  });
});
//-----------------------------------------------------------------
// Validador E-mail
//-----------------------------------------------------------------
$(function() {
  var messageC = $("#text-erromail");
  $('#inputEmail').change(function () {
    var email = $(this).val();
    var validaEmail = /[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}/igm;
    if($(this).val() == '' || $(this).val().length < 5) {
      $(this).css('border', '1px solid red');
      messageC.show();
    }else if(validaEmail.test(email)) {
      $(this).css('border', '1px solid green');
      messageC.hide();
    } else{
      $(this).css('border', '1px solid red');
      messageC.show();
    }
  });
});

//-----------------------------------------------------------------
// Validador Nome
//-----------------------------------------------------------------
$(function() {
  $("#racaPiloto").hide();
  var messageE = $("#text-erronome");
  var imgRaca = $(".modal-body img");
  var nomeRaca = $(".header-nome");
  $('#inputNome').change(function () {
    var nome = $(this).val();
    var validaNome = /^[A-Za-záàâãéèêíïóôõöúçñÁÀÂÃÉÈÍÏÓÔÕÖÚÇÑ'\s-]+/;
    if((this).val() == '' || $(this).val().length < 3 ) {
      $(this).css('border', '1px solid red');
      ('#inputNome').attr('data-target', '');
      messageE.show();

    } else if (validaNome.test(nome)) {
      $(this).css('border', '1px solid green');
      messageE.hide();
      $('#racaPiloto').modal('show');
      $('#inputNome').attr('data-target', '#racaPiloto');
      nomeRaca.text("Mandalore");
      imgRaca.attr('src','assets/img/mandalore.jpg');

    } else {
      $(this).css('border', '1px solid green');
      messageE.hide();
      $('#racaPiloto').modal('show');
      ('#inputNome').attr('data-target', '#racaPiloto');
      nomeRaca.text("Hutta");
      imgRaca.attr('src','assets/img/hutts.jpg');
    }
  });
});
//-----------------------------------------------------------------
// Seleção Espaçonave
//-----------------------------------------------------------------
$(function() {
  $("#naveBox").hide();
  var idPhoto = $("#navePhoto");
  var boxPhoto = $("#naveBox");
  var selectBox = $("#nave-opt");
  $('#nave-opt').change(function(){
    var txtNave =  $('#nave-opt').find('option:selected').text();
    $('#text-nave').val(txtNave);
    if($('#nave-opt').val() == 'nave1') {
      selectBox.css('border', '1px solid green');
      boxPhoto.show();
      idPhoto.attr('src','assets/img/imgnave1.png');
    } else if($('#nave-opt').val() == 'nave2') {
      selectBox.css('border', '1px solid green');
      boxPhoto.show();
      idPhoto.attr('src','assets/img/imgnave2.png');
    }else if($('#nave-opt').val() == 'nave3') {
      selectBox.css('border', '1px solid green');
      boxPhoto.show();
      idPhoto.attr('src','assets/img/imgnave3.png');
    }else if($('#nave-opt').val() == 'nave4') {
      selectBox.css('border', '1px solid green');
      boxPhoto.show();
      idPhoto.attr('src','assets/img/imgnave4.png');
    }else if($('#nave-opt').val() == 'nave5') {
      selectBox.css('border', '1px solid green');
      boxPhoto.show();
      idPhoto.attr('src','assets/img/imgnave5.png');
    }else if($('#nave-opt').val() == 'nave6') {
      selectBox.css('border', '1px solid green');
      boxPhoto.show();
      idPhoto.attr('src','assets/img/imgnave6.png');
    }else if($('#nave-opt').val() == 'nave7') {
      selectBox.css('border', '1px solid green');
      boxPhoto.show();
      idPhoto.attr('src','assets/img/imgnave7.png');
    }else{
      boxPhoto.hide();
    }
  });
});


//-----------------------------------------------------------------
// Envio Formulario
//-----------------------------------------------------------------
("#form-cadastro").submit(function (event) {
  event.preventDefault();
  var erro = false;
  var elemento;

  // limpar marcação de erro da tentativa anterior:
  ('.form-auth').removeClass('erro');
  ('.msg-erro').remove();

  // percorrer todos os input e verificar se estão em branco ou não
  (this).find('.form-auth').each(function () {
    if ((this).val() == '') {
      erro = true;
      elemento = (this);
      return false;
    }
  });

  // aqui pode adicionar outras verificações especifica como combos ou checkbox.
  // definir erro = true e elemento = ao elemento causador do erro.

  if (erro == true) {
    elemento.parent('div').addClass('erro').append((elemento.data('erro') ? '<span class="msg-erro">'+elemento.data('erro')+'</span>' : ''));
    ('html, body').animate({
      scrollTop: elemento.offset().top - 100
    }, 500);
  } else {
    // não tem nenhum erro, fazer os procedimentos de envio
    var result = '';
    for (var i = 15; i > 0; --i) result += (Math.floor(Math.random()*256));
    $("#modalsend").modal('show');
    $("#keyacess").text(result);
  }
});
