<?php
if(isset($_POST['sendform'])) {

    // EDIT THE 2 LINES BELOW AS REQUIRED
    $email_to = "gabrielbradoki@gmail.com";
    $email_subject = "Pilot Registration";

    $id = $_POST['id']; // required
    $name = $_POST['nome']; // required
    $email = $_POST['email']; // required
    $selectOption = $_POST['select']; //required



    $error_message = "";
    $email_exp = '/^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/';


  if(!preg_match($email_exp,$email_from)) {
    $error_message .= 'The Email Address you entered does not appear to be valid.<br />';
  }

    $string_exp = "/^[A-Za-z .'-]+$/";

  if(!preg_match($string_exp,$name)) {
    $error_message .= 'The First Name you entered does not appear to be valid.<br />';
  }

  if(!preg_match($string_exp,$email)) {
    $error_message .= 'The Last Name you entered does not appear to be valid.<br />';
  }

    $email_message = "Informações do formulário Pilot Registration\n\n";


    function clean_string($string) {
      $bad = array("content-type","bcc:","to:","cc:","href");
      return str_replace($bad,"",$string);
    }



    $email_message .= "ID Piloto: ".clean_string($id)."\n";
    $email_message .= "Nome Piloto: ".clean_string($name)."\n";
    $email_message .= "E-mail Piloto: ".clean_string($email)."\n";
    $email_message .= "Espaçonave Selecionada: ".clean_string($selectOption)."\n";

// create email headers
$headers = 'From: '.$email_from."\r\n".
'Reply-To: '.$email_from."\r\n" .
'X-Mailer: PHP/' . phpversion();
@mail($email_to, $email_subject, $email_message, $headers);
?>

<!-- include your own success html here -->

Thank you for contacting us. We will be in touch with you very soon.

<?php

}
?>
